import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { VacationOverviewComponent } from './vacation-overview/vacation-overview.component';
import { VacationService } from './services/vacation.service';
import { HolidayService } from './services/holiday.service';
import { HolidaysComponent } from './holidays/holidays.component';
import { IsRequestedPipe } from './pipes/is-requested.pipe';
import { IsApprovedPipe } from './pipes/is-approved.pipe';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { NewHolidayDialog } from './new-holiday/new-holiday-dialog.component';
import { ReactiveFormsModule } from '@angular/forms';
import { VacationPlannerComponent } from './vacation-planner/vacation-planner.component';
import { WorkTimeStartComponent } from './work-time-start/work-time-start.component';
import { YearNavigationComponent } from './year-navigation/year-navigation.component';
import { NewVacationDialog } from './new-vacation/new-vacation-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    HolidaysComponent,
    IsRequestedPipe,
    IsApprovedPipe,
    WorkTimeStartComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    HttpClientModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    NewHolidayDialog,
    NewVacationDialog,
    ReactiveFormsModule,
    VacationOverviewComponent,
    VacationPlannerComponent,
    YearNavigationComponent,
  ],
  providers: [HolidayService, VacationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
