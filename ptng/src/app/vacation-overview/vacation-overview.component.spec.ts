import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VacationOverviewComponent } from './vacation-overview.component';

describe('VacationOverviewComponent', () => {
  let component: VacationOverviewComponent;
  let fixture: ComponentFixture<VacationOverviewComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [VacationOverviewComponent]
    });
    fixture = TestBed.createComponent(VacationOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
