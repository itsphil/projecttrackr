import { Component } from '@angular/core';
import { VacationService } from '../services/vacation.service';
import { MatTableModule } from '@angular/material/table';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { IVacation } from '../data/IVacation';

@Component({
  selector: 'vacation-overview',
  templateUrl: './vacation-overview.component.html',
  styleUrls: ['./vacation-overview.component.less'],
  standalone: true,
  imports: [
    MatCheckboxModule,
    MatTableModule
  ],
})
export class VacationOverviewComponent {
  displayedColumns: string[] = ['from', 'to', 'comment', 'isRequested', 'isApproved', 'effectiveVacationDays'];
  quota: number = 0
  remainingDays: number = 0;
  vacations: IVacation[] = [];
  year = 2023

  constructor(private _vacationService: VacationService) { }

  ngOnInit() {
    this._vacationService.getQuotaDays(this.year)
      .subscribe(quota => this.quota = quota.numberOfDays);
    this._vacationService.get(this.year)
      .subscribe(vacations => this.vacations = vacations);
    this._vacationService.getRemainingDays(this.year)
      .subscribe(quota => this.remainingDays = quota.numberOfDays);
  }
}
