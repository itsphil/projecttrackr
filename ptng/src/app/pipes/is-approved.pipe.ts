import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'isApproved'
})
export class IsApprovedPipe implements PipeTransform {

  transform(value: boolean) {
    return value ? 'approved' : 'not approved yet';
  }

}
