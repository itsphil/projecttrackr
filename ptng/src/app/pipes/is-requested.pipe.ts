import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'isRequested'
})
export class IsRequestedPipe implements PipeTransform {

  transform(value: boolean) {
    return value ? 'requested' : 'not requested yet';
  }

}
