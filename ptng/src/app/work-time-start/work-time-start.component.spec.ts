import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkTimeStartComponent } from './work-time-start.component';

describe('WorkTimeStartComponent', () => {
  let component: WorkTimeStartComponent;
  let fixture: ComponentFixture<WorkTimeStartComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [WorkTimeStartComponent]
    });
    fixture = TestBed.createComponent(WorkTimeStartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
