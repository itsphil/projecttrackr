import { Component, Input } from '@angular/core';
import { IYearResponse } from '../data/IYearResponse';

import { NgClass, NgFor, NgIf } from '@angular/common';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { WeekdayPipe } from '../weekday.pipe';
import { VacationService } from '../services/vacation.service';
import { YearNavigationComponent } from '../year-navigation/year-navigation.component';
import { Router } from '@angular/router';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { NewHolidayDialog } from '../new-holiday/new-holiday-dialog.component';
import { IHoliday } from '../data/IHoliday';
import { HolidayService } from '../services/holiday.service';
import { NewVacationDialog } from '../new-vacation/new-vacation-dialog.component';
import { IVacation } from '../data/IVacation';

@Component({
  selector: 'app-vacation-planner',
  templateUrl: './vacation-planner.component.html',
  styleUrls: ['./vacation-planner.component.less'],
  standalone: true,
  imports: [
    MatButtonModule,
    MatDialogModule,
    MatGridListModule,
    MatProgressSpinnerModule,
    MatToolbarModule,
    NgClass,
    NgFor,
    NgIf,
    WeekdayPipe,
    YearNavigationComponent,
  ],
})
export class VacationPlannerComponent {
  response: IYearResponse | null = null;
  currentYear = new Date().getFullYear();
  monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
  dayNumbers = new Array(31).fill(null).map((_, i) => i + 1);
  todayYear = new Date().getFullYear();
  todayMonth = new Date().getMonth() + 1;
  todayDay = new Date().getDate();

  constructor(private _holidayService: HolidayService,
    private _vacationService: VacationService,
    private _router: Router,
    private _dialog: MatDialog) { }

  ngOnInit() {
    this.requestForCurrentYear();
    //const d = this._dialog.open(NewVacationDialog);
  }

  @Input()
  set year(year: number | undefined) {
    if (year === undefined) {
      return;
    }

    this.response = null;
    this.currentYear = year;
    this.requestForCurrentYear();
  }

  yearSelected(year: number) {
    // FIXME There must be a better way than this!
    let newUrl = '';
    if (this._router.url.includes(this.currentYear.toString())) {
      newUrl = this._router.url.replace(this.currentYear.toString(), year.toString());
    } else {
      newUrl = this._router.url + '/' + year.toString();
    }
    this._router.navigateByUrl(newUrl);
  }

  requestForCurrentYear() {
    this._vacationService.getOverview(this.currentYear)
      .subscribe(response => this.response = response);
  }

  newHoliday() {
    const d = this._dialog.open(NewHolidayDialog);
    d.afterClosed().subscribe((holiday: Partial<IHoliday>) => {
      this._holidayService.add(holiday)
        .subscribe(() => this.requestForCurrentYear());
    });
  }

  newVacation() {
    const d = this._dialog.open(NewVacationDialog);
    d.afterClosed().subscribe((vacation: Partial<IVacation>) => {
      console.log('Posting ' + JSON.stringify(vacation))
      this._vacationService.add(vacation)
        .subscribe(() => this.requestForCurrentYear());
    });
  }
}
