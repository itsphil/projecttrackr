import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';

@Component({
  selector: 'app-year-navigation',
  templateUrl: './year-navigation.component.html',
  styleUrls: ['./year-navigation.component.less'],
  standalone: true,
  imports: [
    MatButtonModule,
    MatIconModule
  ]
})
export class YearNavigationComponent {

  currentYear = new Date().getFullYear();

  @Input()
  set year(year: number) {
    if (typeof year === 'string') {
      this.currentYear = +year;
      return;
    }
    this.currentYear = year;
  }

  @Output() yearSelected = new EventEmitter<number>();

  incrementAndEmit(by: number) {
    this.currentYear += by;
    this.yearSelected.emit(this.currentYear);
  }

  setTodayAndEmit() {
    this.currentYear = new Date().getFullYear();
    this.yearSelected.emit(this.currentYear);
  }
}
