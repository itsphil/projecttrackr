import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VacationPlannerComponent } from './vacation-planner/vacation-planner.component';
import { VacationOverviewComponent } from './vacation-overview/vacation-overview.component';
import { WorkTimeStartComponent } from './work-time-start/work-time-start.component';

const routes: Routes = [
  { path: 'vacation-planner', component: VacationPlannerComponent },
  { path: 'vacation-planner/:year', component: VacationPlannerComponent },
  { path: 'vacation-overview', component: VacationOverviewComponent },
  { path: 'vacation-overview/:year', component: VacationOverviewComponent },
  { path: '**', component: WorkTimeStartComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { bindToComponentInputs: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
