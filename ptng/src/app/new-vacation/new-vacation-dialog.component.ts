import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { HolidayService } from '../services/holiday.service';
import { IHoliday } from '../data/IHoliday';
import { MatButtonModule } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDialogModule } from '@angular/material/dialog';
import { NgIf } from '@angular/common';
import { MatCheckboxModule } from '@angular/material/checkbox';

@Component({
  selector: 'app-new-vacation-dialog',
  templateUrl: './new-vacation-dialog.component.html',
  styleUrls: ['./new-vacation-dialog.component.less'],
  standalone: true,
  imports: [
    MatButtonModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatDialogModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatInputModule,
    NgIf,
    ReactiveFormsModule
  ]
})
export class NewVacationDialog implements OnInit {
  inputForm!: FormGroup;

  constructor(private _formBuilder: FormBuilder) { }

  ngOnInit() {
    this.inputForm = this._formBuilder.group({
      from: new FormControl<Date>(new Date()).addValidators(Validators.required),
      to: new FormControl<Date>(new Date()).addValidators(Validators.required),
      comment: new FormControl<string>(""),
      isRequested: new FormControl<boolean>(false),
      isApproved: new FormControl<boolean>(false),
      quotaCorrection: new FormControl<number>(0),
    });

    // FIXME Would be nice to fetch effective number of days when changing the dates
  }
}
