export interface IDayResponse {
  day: number,
  weekDay: number,
  kind: string,
  title: string
}
