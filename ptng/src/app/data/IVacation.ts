export interface IVacation {
  id: number,
  from: Date,
  to: Date,
  isRequested: boolean,
  isApproved: boolean,
  quotaCorrection: number,
  comment: string | null,
  effectiveVacationDays: number
}
