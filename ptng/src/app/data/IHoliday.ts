export interface IHoliday {
  date: Date,
  title: string,
  effectOnQuotaInDays: number,
  comment: string | null
}
