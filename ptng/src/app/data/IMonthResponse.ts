import { IDayResponse } from "./IDayResponse";

export interface IMonthResponse {
  month: number,
  days: IDayResponse[]
}
