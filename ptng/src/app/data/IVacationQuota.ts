export interface IVacationQuota {
  year: number,
  numberOfDays: number
}
