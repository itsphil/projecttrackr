import { IMonthResponse } from "./IMonthResponse";

export interface IYearResponse {
  year: number,
  months: IMonthResponse[]
}
