import { Component } from '@angular/core';
import { HolidayService } from '../services/holiday.service';
import { IHoliday } from '../data/IHoliday';

@Component({
  selector: 'holidays',
  templateUrl: './holidays.component.html',
  styleUrls: ['./holidays.component.less']
})
export class HolidaysComponent {
  holidays: IHoliday[] = [];
  year = 2023;

  constructor(private _holidayService: HolidayService) { }

  ngOnInit() {
    this._holidayService.get(this.year)
      .subscribe(holidays => this.holidays = holidays);
  }

}
