import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, of } from 'rxjs';
import { IHoliday } from '../data/IHoliday';

@Injectable({
  providedIn: 'root'
})
export class HolidayService {
  static url = 'http://localhost:4000/api/holiday';

  constructor(private http: HttpClient) { }

  get(year: number): Observable<IHoliday[]> {
    return this.http.get<IHoliday[]>(`${HolidayService.url}/${year}`);
  }

  add(d: Partial<IHoliday>): Observable<Object> {
    return this.http.post(HolidayService.url, d);
  }
}
