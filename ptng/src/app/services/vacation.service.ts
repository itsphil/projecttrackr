import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IVacation } from '../data/IVacation';
import { IVacationQuota } from '../data/IVacationQuota';
import { IYearResponse } from '../data/IYearResponse';

@Injectable({
  providedIn: 'root'
})
export class VacationService {
  static url = 'http://localhost:4000/api/vacation';

  constructor(private http: HttpClient) { }

  add(d: Partial<IVacation>): Observable<Object> {
    return this.http.post(VacationService.url, d);
  }

  get(year: number): Observable<IVacation[]> {
    return this.http.get<IVacation[]>(`${VacationService.url}/${year}`);
  }

  getOverview(year: number): Observable<IYearResponse> {
    return this.http.get<IYearResponse>(`http://localhost:4000/api/overview/${year}`);
  }

  getQuotaDays(year: number): Observable<IVacationQuota> {
    return this.http.get<IVacationQuota>(`${VacationService.url}/${year}/quota`);
  }

  getRemainingDays(year: number): Observable<IVacationQuota> {
    return this.http.get<IVacationQuota>(`${VacationService.url}/${year}/remaining`);
  }
}
