import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'weekday',
  standalone: true
})
export class WeekdayPipe implements PipeTransform {
  weekDayNames = ["Su", "Mo", "Tue", "Wed", "Thu", "Fri", "Sa"];

  transform(value: number, ...args: unknown[]): string {
    if (value < 0 || value >= this.weekDayNames.length) {
      return "invalid";
    }
    return this.weekDayNames[value];
  }

}
