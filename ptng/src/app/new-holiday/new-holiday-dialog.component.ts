import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { HolidayService } from '../services/holiday.service';
import { IHoliday } from '../data/IHoliday';
import { MatButtonModule } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDialogModule } from '@angular/material/dialog';
import { NgIf } from '@angular/common';

@Component({
  selector: 'app-new-holiday-dialog',
  templateUrl: './new-holiday-dialog.component.html',
  styleUrls: ['./new-holiday-dialog.component.less'],
  standalone: true,
  imports: [
    MatButtonModule,
    MatDatepickerModule,
    MatDialogModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatInputModule,
    NgIf,
    ReactiveFormsModule
  ]
})
export class NewHolidayDialog implements OnInit {
  inputForm!: FormGroup;

  constructor(private _formBuilder: FormBuilder) { }

  ngOnInit() {
    this.inputForm = this._formBuilder.group({
      date: new FormControl<Date>(new Date()),
      title: ["", Validators.required],
      effectOnQuotaInDays: 1,
      comment: ""
    });
  }

  get title() { return this.inputForm.get('title')!; }

  get titleValid() { return this.title.invalid && (this.title.dirty || this.title.touched) && this.title.errors?.['required']; }

  get formValid() { return this.inputForm.valid; }
}
