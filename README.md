ProjectTrackr
===

## Getting Started

Install tools from the project root directory
```sh
dotnet tool restore
```

## Stuff

Create database from `/src`
```sh
dotnet tool run dotnet-ef migrations add InitialCreate --project core --context EfBuildContext
dotnet tool run dotnet-ef database update --project core --context EfBuildContext
```
where `EfBuildContext` is used by EF Core to create the migrations and generate a database in `/src/core/pt.db`.

## Console Application

- `./bin/Debug/net7.0/pt add-day-type vacation --db ../pt.db`
