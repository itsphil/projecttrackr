#! /bin/bash
cd core
dotnet-ef migrations add InitialCreate --context EfBuildContext
dotnet-ef database update --context EfBuildContext
cd ..
