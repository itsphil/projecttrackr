using Microsoft.Extensions.Logging;

namespace Pt.Tests;

internal class TestLoggerFactory : LoggerFactory
{
  public static ILoggerFactory Create() => LoggerFactory.Create(builder => { });
}
