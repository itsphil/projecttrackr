using Microsoft.EntityFrameworkCore;
using Pt.Core;
using Pt.Core.Net;
using Xunit;

namespace Pt.Tests;

public class OverviewControllerTests
{
  private static DataContextFactory DcFactory()
  {
    var dcFactory = new DataContextFactory(new DbContextOptionsBuilder<DataContext>()
                                              .UseInMemoryDatabase(Guid.NewGuid().ToString())
                                              .Options);
    var dc = dcFactory.Create();
    dc.Database.EnsureDeleted();
    dc.Database.EnsureCreated();
    return dcFactory;
  }

  private static OverviewController Factory(DataContextFactory? dcFactory = null)
  {
    var dcf = dcFactory ?? DcFactory();
    var lf = new TestLoggerFactory();
    return new OverviewController(dcf, lf);
  }

  #region Get
  public static IEnumerable<object[]> GetNumbersOfDaysData()
  {
    //                                                  01  02  03  04  05  06  07  08  09  10  11  12
    yield return new object[] { 2023, new List<int>() { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 } };
    yield return new object[] { 2024, new List<int>() { 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 } };
  }


  [Theory]
  [MemberData(nameof(GetNumbersOfDaysData))]
  public async Task Get_ReturnsCorrectMonths(int year, List<int> expectedNumberOfDaysPerMonth)
  {
    var dcf = DcFactory();
    var controller = Factory(dcf);

    var dc = dcf.Create();
    dc.Holidays.Add(new Holiday() { Date = new DateOnly(year, 3, 1), Title = "Holiday 1" });
    dc.Holidays.Add(new Holiday() { Date = new DateOnly(year, 4, 2), Title = "Holiday 2" });
    dc.Vacations.Add(new Vacation() { From = new DateOnly(year, 5, 3), To = new DateOnly(2023, 5, 3), Comment = "Vacation 1" });
    dc.Vacations.Add(new Vacation() { From = new DateOnly(year, 12, 31), To = new DateOnly(2023, 12, 31), Comment = "Vacation 2" });

    var data = await controller.Get(year);
    Assert.Equal(expectedNumberOfDaysPerMonth.Sum(), data.months.Sum(month => month.days.Count));
    Assert.Equal(expectedNumberOfDaysPerMonth, data.months.Select(month => month.days.Count));
  }

  [Theory]
  [InlineData(2023, 365)]
  [InlineData(2024, 366)]
  public async Task Get_ReturnsCorrectDayEntries(int year, int expectedNumberOfDays)
  {
    var dcf = DcFactory();
    var controller = Factory(dcf);

    var dc = dcf.Create();
    dc.Holidays.Add(new Holiday() { Date = new DateOnly(year, 3, 1), Title = "Holiday 1" });
    dc.Holidays.Add(new Holiday() { Date = new DateOnly(year, 4, 2), Title = "Holiday 2" });
    dc.Vacations.Add(new Vacation() { From = new DateOnly(year, 5, 3), To = new DateOnly(year, 5, 3), Comment = "Vacation 1" });
    dc.Vacations.Add(new Vacation() { From = new DateOnly(year, 12, 31), To = new DateOnly(year, 12, 31), Comment = "Vacation 2" });
    await dc.SaveChangesAsync();

    var data = await controller.Get(year);
    Assert.Equal(expectedNumberOfDays, data.months.Sum(month => month.days.Count));

    Assert.Equal("", data.months[0].days[0].title);
    Assert.Equal("Free", data.months[0].days[0].kind);

    Assert.Equal("Holiday 1", data.months[2].days[0].title);
    Assert.Equal("Holiday", data.months[2].days[0].kind);
    Assert.Equal("Holiday 2", data.months[3].days[1].title);
    Assert.Equal("Holiday", data.months[3].days[1].kind);

    Assert.Equal("Vacation 1", data.months[4].days[2].title);
    Assert.Equal("Vacation", data.months[4].days[2].kind);
    Assert.Equal("Vacation 2", data.months[11].days[30].title);
    Assert.Equal("Vacation", data.months[11].days[30].kind);
  }

  [Theory]
  [InlineData(2023)]
  [InlineData(2024)]
  public async Task Get_SetsMonthAndDayNumberCorrectly(int year)
  {
    var dcf = DcFactory();
    var controller = Factory(dcf);

    var dc = dcf.Create();
    dc.Holidays.Add(new Holiday() { Date = new DateOnly(year, 3, 1), Title = "Holiday 1" });
    dc.Holidays.Add(new Holiday() { Date = new DateOnly(year, 4, 2), Title = "Holiday 2" });
    dc.Vacations.Add(new Vacation() { From = new DateOnly(year, 5, 3), To = new DateOnly(2023, 5, 3), Comment = "Vacation 1" });
    dc.Vacations.Add(new Vacation() { From = new DateOnly(year, 12, 31), To = new DateOnly(2023, 12, 31), Comment = "Vacation 2" });
    await dc.SaveChangesAsync();

    var data = await controller.Get(year);
    Assert.Equal(year, data.year);

    for (var month = 1; month <= 12; month++)
    {
      Assert.Equal(month, data.months[month - 1].month);
    }

    foreach (var month in data.months)
    {
      for (var day = 1; day <= month.days.Count; day++)
      {
        Assert.Equal(day, month.days[day - 1].day);
      }
    }
  }
  #endregion

}
