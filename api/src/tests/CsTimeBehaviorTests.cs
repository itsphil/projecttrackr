using Xunit;

namespace Pt.Tests;

public class CsTimeBehaviorTests
{
  private DateTime DT(int year, int month, int day, int hour) => new(year, month, day, hour, 0, 0);
  private TimeOnly TO(int hours, int minutes, int seconds) => new(hours, minutes, seconds);
  private TimeSpan TS(int hours, int minutes, int seconds) => new(hours, minutes, seconds);

  [Fact]
  public void DateTimeDifference1()
    => Assert.Equal(TS(1, 0, 0), DT(2023, 8, 28, 14) - DT(2023, 8, 28, 13));

  [Fact]
  public void DateTimeDifference2()
    => Assert.Equal(TS(25, 0, 0), DT(2023, 8, 28, 14) - DT(2023, 8, 27, 13));

  [Fact]
  public void TimeOnlyDifference1()
    => Assert.Equal(TS(6, 6, 6), TO(10, 10, 10) - TO(4, 4, 4));

  [Fact]
  public void TimeSpanDifference()
    => Assert.Equal(TS(-1, -1, -1), TS(10, 10, 10) - TS(11, 11, 11));

  [Fact]
  public void TimeSpanSum()
    => Assert.Equal(TS(26, 0, 0), TS(10, 0, 0) + TS(16, 0, 0));

  [Fact]
  public void TimeSpanToDouble()
    => Assert.Equal(4.0, TS(4, 0, 0).TotalHours);

}
