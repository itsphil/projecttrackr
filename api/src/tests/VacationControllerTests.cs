using Microsoft.EntityFrameworkCore;
using Pt.Core;
using Pt.Core.Net;
using Xunit;

namespace Pt.Tests;

using Payload = VacationPayload;

public class VacationControllerTests
{

  private static DataContextFactory DcFactory()
  {
    var dcFactory = new DataContextFactory(new DbContextOptionsBuilder<DataContext>()
                                              .UseInMemoryDatabase(Guid.NewGuid().ToString())
                                              .Options);
    var dc = dcFactory.Create();
    dc.Database.EnsureDeleted();
    dc.Database.EnsureCreated();
    return dcFactory;
  }

  private static VacationController Factory(DataContextFactory? dcFactory = null)
  {
    var dcf = dcFactory ?? DcFactory();
    var lf = new TestLoggerFactory();
    return new VacationController(dcf, lf);
  }

  public static IEnumerable<object[]> GetValidPayloads()
  {
    // Values: new payload, expected payload
    yield return new object[] { "a)", new Payload() { id = 1, from = "2023-02-03", to = "2023-02-03",   quotaCorrection =  1,   isRequested = true,   isApproved = true,   comment = "asdf"   },
                                      new Payload() { id = 1, from = "2023-02-03", to = "2023-02-03",   quotaCorrection =  1,   isRequested = true,   isApproved = true,   comment = "asdf"   } };
    yield return new object[] { "b)", new Payload() { id = 1, from = "2023-02-03", to = "2023-02-03", /*quotaCorrection =  1,*/ isRequested = true,   isApproved = true,   comment = "asdf"   },
                                      new Payload() { id = 1, from = "2023-02-03", to = "2023-02-03",   quotaCorrection =  0,   isRequested = true,   isApproved = true,   comment = "asdf"   } };
    yield return new object[] { "c)", new Payload() { id = 1, from = "2023-02-03", to = "2023-02-03",   quotaCorrection =  1, /*isRequested = true,*/ isApproved = true,   comment = "asdf"   },
                                      new Payload() { id = 1, from = "2023-02-03", to = "2023-02-03",   quotaCorrection =  1,   isRequested = false,  isApproved = true,   comment = "asdf"   } };
    yield return new object[] { "d)", new Payload() { id = 1, from = "2023-02-03", to = "2023-02-03",   quotaCorrection =  1,   isRequested = true, /*isApproved = true,*/ comment = "asdf"   },
                                      new Payload() { id = 1, from = "2023-02-03", to = "2023-02-03",   quotaCorrection =  1,   isRequested = true,   isApproved = false,  comment = "asdf"   } };
    yield return new object[] { "e)", new Payload() { id = 1, from = "2023-02-03", to = "2023-02-03",   quotaCorrection =  1,   isRequested = true,   isApproved = true, /*comment = "asdf"*/ },
                                      new Payload() { id = 1, from = "2023-02-03", to = "2023-02-03",   quotaCorrection =  1,   isRequested = true,   isApproved = true,   comment = null     } };
  }

  public static IEnumerable<object?[]> GetInvalidPayloadsToAdd()
  {
    yield return new object?[] { "a)", null };
    yield return new object?[] { "b)", new Payload() }; // Empty payload
    yield return new object?[] { "c)", new Payload() { id = 1 } }; // Only ID set (dates are missing)
    yield return new object?[] { "d)", new Payload() { from = null, to = "2023-02-03" } }; // From date cannot be parsed
    yield return new object?[] { "e)", new Payload() { from = "", to = "2023-02-03" } }; // From date cannot be parsed
    yield return new object?[] { "f)", new Payload() { from = "asdf", to = "2023-02-03" } }; // From date cannot be parsed
    yield return new object?[] { "g)", new Payload() { from = "2023-02-03", to = null } }; // To date cannot be parsed
    yield return new object?[] { "h)", new Payload() { from = "2023-02-03", to = "" } }; // To date cannot be parsed
    yield return new object?[] { "i)", new Payload() { from = "2023-02-03", to = "asdf" } }; // To date cannot be parsed
    yield return new object?[] { "j)", new Payload() { from = "2023-02-03", to = "2023-02-01" } }; // To is before from
    yield return new object?[] { "k)", new Payload() { from = "2023-02-03", to = "2023-02-03", quotaCorrection = 1.1m } }; // Quota correction is too large
    yield return new object?[] { "l)", new Payload() { from = "2023-02-03", to = "2023-02-03", quotaCorrection = -1 } }; // Quota correction is negative
                                                                                                                         // All other fields are optional
  }

  public static IEnumerable<object[]> GetPayloadsToUpdate()
  {
    //     payload to update (omit property not to update), expected payload after applying the update (compare with first valid payload)
    // The ID cannot be updated but will be ignored if set
    yield return new object[] { "a)", new Payload(),
                                      new Payload() { id = 1,   from = "2023-02-03",     to = "2023-02-03",     quotaCorrection = 1,   isRequested = true,    isApproved = true,    comment = "asdf"  } };
    yield return new object[] { "b)", new Payload() { id = 2, /*from = "2023-02-04",*/   to = "2023-02-05",     quotaCorrection = 0,   isRequested = false,   isApproved = false,   comment = "ddd"   },
                                      new Payload() { id = 1,   from = "2023-02-03",     to = "2023-02-05",     quotaCorrection = 0,   isRequested = false,   isApproved = false,   comment = "ddd"   } };
    yield return new object[] { "c)", new Payload() { id = 2,   from = "2023-02-02",   /*to = "2023-02-05",*/   quotaCorrection = 0,   isRequested = false,   isApproved = false,   comment = "ddd"   },
                                      new Payload() { id = 1,   from = "2023-02-02",     to = "2023-02-03",     quotaCorrection = 0,   isRequested = false,   isApproved = false,   comment = "ddd"   } };
    yield return new object[] { "d)", new Payload() { id = 2,   from = "2023-02-04",     to = "2023-02-05",   /*quotaCorrection = 0,*/ isRequested = false,   isApproved = false,   comment = "ddd"   },
                                      new Payload() { id = 1,   from = "2023-02-04",     to = "2023-02-05",     quotaCorrection = 1,   isRequested = false,   isApproved = false,   comment = "ddd"   } };
    yield return new object[] { "e)", new Payload() { id = 2,   from = "2023-02-04",     to = "2023-02-05",     quotaCorrection = 0, /*isRequested = false,*/ isApproved = false,   comment = "ddd"   },
                                      new Payload() { id = 1,   from = "2023-02-04",     to = "2023-02-05",     quotaCorrection = 0,   isRequested = true,    isApproved = false,   comment = "ddd"   } };
    yield return new object[] { "f)", new Payload() { id = 2,   from = "2023-02-04",     to = "2023-02-05",     quotaCorrection = 0,   isRequested = false, /*isApproved = false,*/ comment = "ddd"   },
                                      new Payload() { id = 1,   from = "2023-02-04",     to = "2023-02-05",     quotaCorrection = 0,   isRequested = false,   isApproved = true,    comment = "ddd"   } };
    yield return new object[] { "g)", new Payload() { id = 2,   from = "2023-02-04",     to = "2023-02-05",     quotaCorrection = 0,   isRequested = false,   isApproved = false, /*comment = "ddd"*/ },
                                      new Payload() { id = 1,   from = "2023-02-04",     to = "2023-02-05",     quotaCorrection = 0,   isRequested = false,   isApproved = false,   comment = "asdf"  } };
    yield return new object[] { "h)", new Payload() { id = 2,   from = "2023-02-04",     to = "2023-02-05",     quotaCorrection = 0,   isRequested = false,   isApproved = false,   comment = "ddd"   },
                                      new Payload() { id = 1,   from = "2023-02-04",     to = "2023-02-05",     quotaCorrection = 0,   isRequested = false,   isApproved = false,   comment = "ddd"   } };
  }

  public static IEnumerable<object?[]> GetInvalidPayloadsToUpdate()
  {
    yield return new object?[] { "a)", null };
    yield return new object?[] { "b)", new Payload() { from = "2023-02-04" } }; // From date is after existing to date
    yield return new object?[] { "c)", new Payload() { to = "2023-02-02" } }; // To date is before existing from date
    yield return new object?[] { "d)", new Payload() { from = "2023-02-03", to = "2023-02-01" } }; // To is before from date
    yield return new object?[] { "e)", new Payload() { quotaCorrection = 1.1m } }; // Quota correction is too large
    yield return new object?[] { "f)", new Payload() { quotaCorrection = -1 } }; // Quota correction is negative
                                                                                 // All other fields are not updated if not set
  }

  public static IEnumerable<object?[]> GetInvalidKeys()
  {
    // Invalid keys are values that cannot be parsed as date
    yield return new object[] { -1 }; // Invalid range
    yield return new object[] { 1 }; // Valid range but does not exist in empty database
  }

  public static IEnumerable<object[]> GetRangesToTest()
  {
    yield return new object[]
    {
      "a) Three days with no weekend in between",
      new Vacation() { From = new DateOnly(2023, 10, 10),
                       To = new DateOnly(2023, 10, 12) },
      3m
    };
    yield return new object[]
    {
      "b) Two days with weekend in between",
      new Vacation { From = new DateOnly(2023, 10, 6),
                     // 2023-10-07 is Saturday
                     // 2023-10-08 is Sunday
                     To = new DateOnly(2023, 10, 9) } ,
      2m
    };
    yield return new object[]
    {
      "c) Two days with holiday in between",
      new Vacation { From = new DateOnly(2023, 10, 2) ,
                     // 2023-10-03 is holiday (Unity)
                     To = new DateOnly(2023, 10, 4) },
      2m
    };
    yield return new object[]
      {
      "d) Three days across months",
      new Vacation() { From = new DateOnly(2023, 7, 31),
                       To = new DateOnly(2023, 8, 2) },
      3m
      };
    yield return new object[]
    {
      "e) Single day",
      new Vacation() { From = new DateOnly(2023, 10, 17),
                       To = new DateOnly(2023, 10, 17) },
      1m
    };
    yield return new object[]
    {
      "f) Three days with weekend and holiday in between",
      new Vacation() { From = new DateOnly(2023, 11, 3),
                       // 2023-11-04 is Saturday
                       // 2023-11-05 is Sunday
                       // 2023-11-07 is holiday (After weekend)
                       To = new DateOnly(2023, 11, 8) },
      3m
    };
    yield return new object[]
    {
      "g) Three days with weekend and holiday in between and correction entry",
      new Vacation() { From = new DateOnly(2023, 11, 3),
                       // 2023-11-04 is Saturday
                       // 2023-11-05 is Sunday
                       // 2023-11-07 is holiday (After weekend)
                       To = new DateOnly(2023, 11, 8),
                       QuotaCorrection = 0.5m },
      2.5m
    };
  }

  private static void AssertPropertiesEqual(Payload expected, Payload given)
  {
    Assert.Equal(expected.from, given.from);
    Assert.Equal(expected.to, given.to);
    Assert.Equal(expected.quotaCorrection, given.quotaCorrection);
    Assert.Equal(expected.isRequested, expected.isRequested);
    Assert.Equal(expected.isApproved, given.isApproved);
    Assert.Equal(expected.comment, given.comment);
  }

  #region Add
  [Theory]
  [MemberData(nameof(GetValidPayloads))]
  public async Task Add_WorksWithValidPayload(string id, Payload newPayload, Payload expectedPayload)
  {
    var controller = Factory();
    await controller.Add(newPayload);
    var writtenData = await controller.Get(2023);
    Assert.Single(writtenData);
    AssertPropertiesEqual(expectedPayload, writtenData.First());
  }

  [Theory]
  [MemberData(nameof(GetInvalidPayloadsToAdd))]
  public async Task Add_FailsWithInvalidPayload(string id, Payload? payload)
  {
    var controller = Factory();
    await Assert.ThrowsAsync<ArgumentException>(async () => await controller.Add(payload));
  }
  #endregion

  #region Delete
  [Fact]
  public async Task Delete_WorksWithValidKey()
  {
    var controller = Factory();
    var payload = (Payload)GetValidPayloads().First()[1];
    await controller.Add(payload);
    await controller.Delete(payload.id!.Value);
    Assert.Empty(await controller.Get(2023));
  }

  [Theory]
  [MemberData(nameof(GetInvalidKeys))]
  public async Task Delete_FailsWithInvalidKey(int key)
  {
    var controller = Factory();
    await Assert.ThrowsAsync<ArgumentException>(async () => await controller.Delete(key));
  }
  #endregion

  #region Get
  [Fact]
  public async Task Get_ReturnsNothingWithoutData()
  {
    var controller = Factory();
    var data = await controller.Get(2023);
    Assert.Empty(data);
  }
  #endregion

  #region GetRemainingDays
  [Theory]
  [MemberData(nameof(GetRangesToTest))]
  public async Task GetRemainingDays_ReturnsCorrectValueWithVacations(string id, Vacation v, decimal expectedNumberOfVacationDays)
  {
    var dcf = DcFactory();
    var controller = Factory(dcf);

    var dc = dcf.Create();
    await dc.VacationQuotas.AddAsync(new VacationQuota() { Year = 2023, NumberOfDays = 30 });
    await dc.Vacations.AddAsync(v);
    await dc.Holidays.AddAsync(new Holiday() { Date = new DateOnly(2023, 10, 3), Title = "Unity" });
    await dc.Holidays.AddAsync(new Holiday() { Date = new DateOnly(2023, 11, 7), Title = "After weekend" });
    await dc.SaveChangesAsync();

    var result = await controller.GetRemainingDays(2023);
    Assert.Equal(2023, result.year);
    Assert.Equal(expectedNumberOfVacationDays, 30 - result.numberOfDays);
  }

  [Fact]
  public async Task GetRemainingDays_ReturnsCorrectValueWithoutVacations()
  {
    var dcf = DcFactory();
    var controller = Factory(dcf);

    var dc = dcf.Create();
    await dc.VacationQuotas.AddAsync(new VacationQuota() { Year = 2023, NumberOfDays = 30 });
    await dc.SaveChangesAsync();

    var result = await controller.GetRemainingDays(2023);
    Assert.Equal(2023, result.year);
    Assert.Equal(30, result.numberOfDays);
  }

  [Fact]
  public async Task GetRemainingDays_FailsWithoutQuota()
  {
    var controller = Factory();
    await Assert.ThrowsAsync<ArgumentException>(async () => await controller.GetRemainingDays(2023));
  }
  #endregion

  #region Update
  [Theory]
  [MemberData(nameof(GetInvalidPayloadsToUpdate))]
  public async Task Update_FailsWithinvalidPayloads(string id, Payload? payload)
  {
    var controller = Factory();
    var payloadToAdd = (Payload)GetValidPayloads().First()[1];
    await controller.Add(payloadToAdd);
    await Assert.ThrowsAsync<ArgumentException>(async () => await controller.Update(payloadToAdd.id!.Value, payload));
  }

  [Theory]
  [MemberData(nameof(GetInvalidKeys))]
  public async Task Update_FailsWithInvalidKey(int key)
  {
    var controller = Factory();
    var newPayload = new Payload() { from = "2023-03-03", to = "2023-03-03", quotaCorrection = -43, isRequested = false, isApproved = false, comment = "ddd" };
    await Assert.ThrowsAsync<ArgumentException>(async () => await controller.Update(key, newPayload));
  }

  [Theory]
  [MemberData(nameof(GetPayloadsToUpdate))]
  public async Task Update_DoesNotUpdatePropertyIfItIsNotSet(string id, Payload updatedPayload, Payload expectedPayloadAfterUpdate)
  {
    var controller = Factory();
    var payload = (Payload)GetValidPayloads().First()[1];
    await controller.Add(payload);

    await controller.Update(payload.id!.Value, updatedPayload);

    var writtenData = await controller.Get(2023);
    Assert.Single(writtenData);
    AssertPropertiesEqual(expectedPayloadAfterUpdate, writtenData.First());
  }
  #endregion
}
