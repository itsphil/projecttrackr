using Microsoft.EntityFrameworkCore;
using Pt.Core;
using Pt.Core.Net;
using Xunit;

namespace Pt.Tests;

using Payload = HolidayPayload;

public class HolidayControllerTests
{
  private HolidayController Factory()
  {
    var dcFactory = new DataContextFactory(new DbContextOptionsBuilder<DataContext>()
                                              .UseInMemoryDatabase(Guid.NewGuid().ToString())
                                              .Options);
    var dc = dcFactory.Create();
    dc.Database.EnsureDeleted();
    dc.Database.EnsureCreated();

    var lf = new TestLoggerFactory();

    return new HolidayController(dcFactory, lf);
  }

  public static IEnumerable<object[]> GetValidPayloads()
  {
    // Values: new payload, expected payload
    yield return new object[] { new Payload() { date = "2023-02-03",   title = "title",   partOfFullDay = 12,   comment = "asdf"   },
                                new Payload() { date = "2023-02-03",   title = "title",   partOfFullDay = 12,   comment = "asdf"   } };
    yield return new object[] { new Payload() { date = "2023-02-03",   title = "title", /*partOfFullDay = 12,*/ comment = "asdf"   },
                                new Payload() { date = "2023-02-03",   title = "title",   partOfFullDay = 1,    comment = "asdf"   } };
    yield return new object[] { new Payload() { date = "2023-02-03",   title = "title",   partOfFullDay = 12  /*comment = "asdf"*/ },
                                new Payload() { date = "2023-02-03",   title = "title",   partOfFullDay = 12,   comment = null     } };
  }

  public static IEnumerable<object?[]> GetInvalidPayloads()
  {
    yield return new object?[] { null };
    yield return new object?[] { new Payload() }; // Empty payload
    yield return new object?[] { new Payload() { date = null } }; // Date cannot be parsed
    yield return new object?[] { new Payload() { date = "" } }; // Date cannot be parsed
    yield return new object?[] { new Payload() { date = "asdf" } }; // Date cannot be parsed
    // All other fields are optional
  }

  public static IEnumerable<object[]> GetPayloadsToUpdate()
  {
    //     payload to update (omit property not to update), expected payload after applying the update
    yield return new object[] { new Payload(),
                                new Payload() {   date = "2023-02-03",     title = "title",   partOfFullDay = 13,   comment = "asdf"  } };
    yield return new object[] { new Payload() { /*date = "2023-02-03",*/ /*title = "dd",*/    partOfFullDay = 13,   comment = "ddd"   },
                                new Payload() {   date = "2023-02-03",     title = "title",   partOfFullDay = 13,   comment = "ddd"   } };
    yield return new object[] { new Payload() { /*date = "2023-02-03",*/   title = "dd",    /*partOfFullDay = 13,*/ comment = "ddd"   },
                                new Payload() {   date = "2023-02-03",     title = "dd",      partOfFullDay = 12,   comment = "ddd"   } };
    yield return new object[] { new Payload() { /*date = "2023-02-03",*/   title = "dd",      partOfFullDay = 13, /*comment = "ddd"*/ },
                                new Payload() {   date = "2023-02-03",     title = "dd",      partOfFullDay = 13,   comment = "asdf"  } };
  }

  public static IEnumerable<object?[]> GetInvalidKeys()
  {
    // Invalid keys are values that cannot be parsed as date
    yield return new object?[] { null };
    yield return new object?[] { "" };
    yield return new object?[] { "asdf" };
    yield return new object?[] { "2021-05-11" }; // Date does not exist
  }

  private static void AssertPropertiesEqual(Payload expected, Payload given)
  {
    Assert.Equal(expected.date, given.date);
    Assert.Equal(expected.title, given.title);
    Assert.Equal(expected.partOfFullDay, expected.partOfFullDay);
    Assert.Equal(expected.comment, given.comment);
  }

  #region Add
  [Theory]
  [MemberData(nameof(GetValidPayloads))]
  public async Task Add_WorksWithValidPayload(Payload newPayload, Payload expectedPayload)
  {
    var controller = Factory();
    await controller.Add(newPayload);
    var writtenData = await controller.Get(2023);
    Assert.Single(writtenData);
    AssertPropertiesEqual(expectedPayload, writtenData.First());
  }

  [Theory]
  [MemberData(nameof(GetInvalidPayloads))]
  public async Task Add_FailsWithInvalidPayload(Payload payload)
  {
    var controller = Factory();
    await Assert.ThrowsAsync<ArgumentException>(async () => await controller.Add(payload));
  }
  #endregion

  #region Delete
  [Fact]
  public async Task Delete_WorksWithValidKey()
  {
    var controller = Factory();
    var payload = (Payload)GetValidPayloads().First().First();
    await controller.Add(payload);
    await controller.Delete(payload.date!);
    Assert.Empty(await controller.Get(2023));
  }

  [Theory]
  [MemberData(nameof(GetInvalidKeys))]
  public async Task Delete_FailsWithInvalidKey(string key)
  {
    var controller = Factory();
    await Assert.ThrowsAsync<ArgumentException>(async () => await controller.Delete(key));
  }
  #endregion

  #region Get
  [Fact]
  public async Task Get_ReturnsNothingWithoutData()
  {
    var controller = Factory();
    var data = await controller.Get(2023);
    Assert.Empty(data);
  }
  #endregion

  #region Update
  [Fact]
  public async Task Update_WorksWithValidPayload()
  {
    var controller = Factory();
    var originalPayload = (Payload)GetValidPayloads().First().First();
    await controller.Add(originalPayload);

    var newPayload = new Payload() { title = "new", partOfFullDay = 48, comment = "ddd" };
    await controller.Update(originalPayload.date!, newPayload);

    var writtenData = await controller.Get(2023);
    Assert.Single(writtenData);
    AssertPropertiesEqual(new Payload() { date = originalPayload.date, title = "new", partOfFullDay = 48, comment = "ddd" },
                          writtenData.First());
  }

  [Fact]
  public async Task Update_FailsWithoutPayload()
  {
    var controller = Factory();
    var payload = (Payload)GetValidPayloads().First().First();
    await controller.Add(payload);
    await Assert.ThrowsAsync<ArgumentException>(async () => await controller.Update(payload.date!, null));
  }

  [Fact]
  public async Task Update_FailsWithPrimaryKeySet()
  {
    var controller = Factory();
    var payload = (Payload)GetValidPayloads().First().First();
    await controller.Add(payload);

    var newPayload = new Payload() { date = "2024-01-01" };
    await Assert.ThrowsAsync<ArgumentException>(async () => await controller.Update(payload.date!, newPayload));
  }

  [Theory]
  [MemberData(nameof(GetInvalidKeys))]
  public async Task Update_FailsWithInvalidKey(string key)
  {
    var controller = Factory();
    var newPayload = new Payload() { title = "new", partOfFullDay = 48, comment = "ddd" };
    await Assert.ThrowsAsync<ArgumentException>(async () => await controller.Update(key, newPayload));
  }

  [Theory]
  [MemberData(nameof(GetPayloadsToUpdate))]
  public async Task Update_DoesNotUpdatePropertyIfValueIsNull(Payload updatedPayload, Payload expectedPayloadAfterUpdate)
  {
    var controller = Factory();
    var payload = (Payload)GetValidPayloads().First().First();
    await controller.Add(payload);

    await controller.Update(payload.date!, updatedPayload);

    var writtenData = await controller.Get(2023);
    Assert.Single(writtenData);
    AssertPropertiesEqual(expectedPayloadAfterUpdate, writtenData.First());
  }
  #endregion
}
