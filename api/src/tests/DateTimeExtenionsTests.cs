using Pt.Core;
using Xunit;

namespace Pt.Tests;

public class DateTimeExtenionsTests
{
  public static IEnumerable<object[]> GetDaysBetweenData()
  {
    yield return new object[] { "a)", new DateOnly(2023, 10, 10), new DateOnly(2023, 10, 10), new List<DateOnly>() };
    yield return new object[] { "b)", new DateOnly(2023, 10, 10), new DateOnly(2023, 10, 12), new List<DateOnly>() { new(2023, 10, 11) } };
    yield return new object[] { "c)", new DateOnly(2023, 10, 31), new DateOnly(2023, 11, 2), new List<DateOnly>() { new(2023, 11, 1) } };
    yield return new object[] { "d)", new DateOnly(2023, 10, 10), new DateOnly(2023, 10, 8), new List<DateOnly>() { new(2023, 10, 9) } };
  }

  [Theory]
  [MemberData(nameof(GetDaysBetweenData))]
  public void DaysBetween_ReturnsCorrectValues(string _, DateOnly from, DateOnly to, List<DateOnly> expectedDays)
  {
    var givenDays = from.DaysBetween(to).ToList();
    Assert.Equal(expectedDays, givenDays);
  }

  public static IEnumerable<object[]> GetTotalDaysToData()
  {
    yield return new object[] { "a)", new DateOnly(2023, 10, 10), new DateOnly(2023, 10, 10), 1m };
    yield return new object[] { "b)", new DateOnly(2023, 10, 10), new DateOnly(2023, 10, 12), 3m };
  }

  [Theory]
  [MemberData(nameof(GetTotalDaysToData))]
  public void TotalDaysTo_ReturnsCorrectValues(string _, DateOnly from, DateOnly to, decimal expectedNumberOfDays)
  {
    var givenNumberOfDays = from.TotalDaysTo(to);
    Assert.Equal(expectedNumberOfDays, givenNumberOfDays);
  }
}
