using Microsoft.EntityFrameworkCore;
using Pt.Core;
using Pt.Core.Net;
using Pt.Tests;
using Xunit;
using static Pt.Core.Net.VacationQuotaController;

namespace Pt.Tests;

public class VacationQuotaControllerTests
{
  private VacationQuotaController Factory()
  {
    var dcFactory = new DataContextFactory(new DbContextOptionsBuilder<DataContext>()
                                              .UseInMemoryDatabase(Guid.NewGuid().ToString())
                                              .Options);
    var dc = dcFactory.Create();
    dc.Database.EnsureDeleted();
    dc.Database.EnsureCreated();

    var lf = new TestLoggerFactory();

    return new VacationQuotaController(dcFactory, lf);
  }

  public static IEnumerable<object[]> GetValidPayloads()
  {
    // Values: new payload, expected payload
    yield return new object[] { new Payload() { year = 2023, numberOfDays = 42 }, new Payload() { year = 2023, numberOfDays = 42 } };
  }

  public static IEnumerable<object?[]> GetInvalidPayloads()
  {
    yield return new object?[] { null };
    yield return new object?[] { new Payload() };
    yield return new object?[] { new Payload() { year = 2023 } };
    yield return new object?[] { new Payload() { numberOfDays = 42 } };
  }

  public static IEnumerable<object[]> GetPayloadsToUpdate()
  {
    //     payload to update (omit property not to update), expected payload after applying the update
    yield return new object[] { new Payload(), new Payload() { year = 2023, numberOfDays = 42 } };
  }

  public static IEnumerable<object?[]> GetInvalidKeys()
  {
    // Key is integer, but a value that does not exist must fail
    yield return new object?[] { 2023 };
  }

  private static void AssertPropertiesEqual(Payload expected, Payload given)
  {
    Assert.Equal(expected.year, given.year);
    Assert.Equal(expected.numberOfDays, given.numberOfDays);
  }

  #region Add
  [Theory]
  [MemberData(nameof(GetValidPayloads))]
  public async Task Add_WorksWithValidPayload(Payload newPayload, Payload expectedPayload)
  {
    var controller = Factory();
    await controller.Add(newPayload);
    var writtenData = await controller.Get(2023);
    AssertPropertiesEqual(expectedPayload, writtenData);
  }

  [Theory]
  [MemberData(nameof(GetInvalidPayloads))]
  public async Task Add_FailsWithInvalidPayload(Payload? payload)
  {
    var controller = Factory();
    await Assert.ThrowsAsync<ArgumentException>(async () => await controller.Add(payload));
  }
  #endregion

  #region Delete
  [Fact]
  public async Task Delete_WorksWithValidKey()
  {
    var controller = Factory();
    await controller.Add((Payload)GetValidPayloads().First().First());
    await controller.Delete(2023);
    await Assert.ThrowsAsync<ArgumentException>(async () => await controller.Get(2023));
  }

  [Theory]
  [MemberData(nameof(GetInvalidKeys))]
  public async Task Delete_FailsWithInvalidKey(int key)
  {
    var controller = Factory();
    await Assert.ThrowsAsync<ArgumentException>(async () => await controller.Delete(key));
  }
  #endregion

  #region Get
  [Fact]
  public async Task Get_FailsWithoutData()
  {
    var controller = Factory();
    await Assert.ThrowsAsync<ArgumentException>(async () => await controller.Get(2023));
  }
  #endregion

  #region Update
  [Fact]
  public async Task Update_WorksWithValidPayload()
  {
    var controller = Factory();
    await controller.Add((Payload)GetValidPayloads().First().First());

    var newPayload = new Payload() { numberOfDays = 44 };
    await controller.Update(2023, newPayload);

    var writtenData = await controller.Get(2023);
    Assert.Equal(44, writtenData.numberOfDays);
  }

  [Fact]
  public async Task Update_FailsWithoutPayload()
  {
    var controller = Factory();
    await controller.Add((Payload)GetValidPayloads().First().First());
    await Assert.ThrowsAsync<ArgumentException>(async () => await controller.Update(2023, null));
  }

  [Fact]
  public async Task Update_FailsWithPrimaryKeySet()
  {
    var controller = Factory();
    await controller.Add((Payload)GetValidPayloads().First().First());

    var newPayload = new Payload() { year = 2344 };
    await Assert.ThrowsAsync<ArgumentException>(async () => await controller.Update(2023, newPayload));
  }

  [Theory]
  [MemberData(nameof(GetInvalidKeys))]
  public async Task Update_FailsWithInvalidKey(int key)
  {
    var controller = Factory();
    var newPayload = new Payload() { year = 2344 };
    await Assert.ThrowsAsync<ArgumentException>(async () => await controller.Update(key, newPayload));
  }

  [Theory]
  [MemberData(nameof(GetPayloadsToUpdate))]
  public async Task Update_DoesNotUpdatePropertyIfValueIsNull(Payload updatedPayload, Payload expectedPayloadAfterUpdate)
  {
    var controller = Factory();
    await controller.Add(expectedPayloadAfterUpdate);

    await controller.Update(2023, updatedPayload);

    var writtenData = await controller.Get(2023);
    AssertPropertiesEqual(expectedPayloadAfterUpdate, writtenData);
  }
  #endregion
}
