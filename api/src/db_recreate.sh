#! /bin/bash
rm ../pt.db*
rm -rf core/Migrations
cd core
dotnet tool restore
dotnet-ef migrations add InitialCreate --context EfBuildContext
dotnet-ef database update --context EfBuildContext
cd ..
