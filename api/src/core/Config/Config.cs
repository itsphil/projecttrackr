using System.Text.Json.Serialization;

namespace Pt.Core;

public class DatabaseConfig
{
  public string FilePath { get; set; } = "";
}

public class ServerConfig
{
  public UInt16 Port { get; set; } = 0;
}

public class WorkTimeConfig
{
  public decimal WeeklyWorkHours { get; set; } = 40;
  public decimal MinimumHoursForAutomaticBreak { get; set; } = 6;
  public decimal BreakDuration { get; set; } = 0.45m;

  [JsonIgnore] public decimal HoursPerDay => WeeklyWorkHours / 5;
}

public class Config
{
  public DatabaseConfig Database { get; set; } = new();
  public ServerConfig Server { get; set; } = new();
  public WorkTimeConfig WorkTime { get; set; } = new();
}
