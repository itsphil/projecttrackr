namespace Pt.Core;

public class ConfigService
{
  private readonly Config _config;

  private ConfigService(Config config)
  {
    _config = config;
  }

  public static ConfigService CreateWithDefaultConfig() => new ConfigService(new Config());

  public static ConfigService CreateWithConfig(Config config) => new ConfigService(config);

  public T Get<T>(Func<Config, T> get) => get(_config);
}
