namespace Pt.Core.Private;

internal class DataAccessServicePrivate
{

  // private readonly DataContext _dc;

  // public DataAccessServicePrivate(DataContext dc)
  // {
  //   _dc = dc;
  // }

  // public TimeSpan ProjectTime(Project project, DateTime from, DateTime to)
  //   => DaysInRange(from, to).Select(day => SumWithBreak(day))
  //                           .Sum();

  // internal IEnumerable<Day> DaysInRange(DateTime from, DateTime to)
  //   => _dc.Days.Where(day => day.Date.DayBeginning() >= from && day.Date.DayEnd() <= to);

  // private IEnumerable<TrackingEntry> TrackingEntriesForDay(Day day)
  // => _dc.TrackingEntries.Where(x => x.Day == day);

  // internal TimeSpan SumWithBreak(Day day)
  // {
  //   var sumWithoutBreak = TrackingEntriesForDay(day).Select(te => day.Date.ToDateTime(te.To) - day.Date.ToDateTime(te.From))
  //                                                   .Sum();
  //   if (sumWithoutBreak > day.BreakPolicy.SubtractionThreshold)
  //   {
  //     return sumWithoutBreak - day.BreakPolicy.BreakTime;
  //   }

  //   return sumWithoutBreak;
  // }
}
