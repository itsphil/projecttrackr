using Pt.Core.Private;

namespace Pt.Core;

public class DataAccessService
{

  // private readonly DataContext _dc;
  // private readonly DataAccessServicePrivate _p;

  // public DataAccessService(DataContext dc)
  // {
  //   _dc = dc;
  //   _p = new(dc);
  // }

  // public class Report
  // {
  //   public List<Tuple<Project, TimeSpan>> ProjectTime { get; set; } = new();
  //   public TimeSpan TotalSum { get; set; }
  // }

  // public class MonthOverviewReportEntry
  // {
  //   public Day Day { get; }
  //   public IEnumerable<TrackingEntry> TrackingEntries { get; }
  //   public TimeSpan Sum { get; }

  //   public MonthOverviewReportEntry(Day day, IEnumerable<TrackingEntry> trackingEntries, TimeSpan sum)
  //   {
  //     Day = day;
  //     TrackingEntries = trackingEntries;
  //     Sum = sum;
  //   }
  // }

  // public class OvertimeReport
  // {
  //   public int Year { get; }
  //   public int Week { get; }
  //   public TimeSpan RequiredTime { get; }
  //   public TimeSpan ActualTime { get; }
  //   public TimeSpan Overtime { get; }

  //   public OvertimeReport(int week, TimeSpan requiredTime, TimeSpan actualTime, TimeSpan overtime)
  //   {
  //     Week = week;
  //     RequiredTime = requiredTime;
  //     ActualTime = actualTime;
  //     Overtime = overtime;
  //   }
  // }

  // public IEnumerable<MonthOverviewReportEntry> MonthOverview(int year, int month)
  // {
  //   var from = new DateOnly(year, month, 1).DayBeginning();
  //   var to = new DateOnly(year, month, 31).DayEnd(); // FIXME determine last day of month

  //   var days = _p.DaysInRange(from, to);
  //   var ret = new List<MonthOverviewReportEntry>();
  //   foreach (var day in days)
  //   {
  //     var trackingEntries = _dc.TrackingEntries.Where(te => te.Day == day);
  //     var sum = _p.SumWithBreak(day);
  //     var entry = new MonthOverviewReportEntry(day, trackingEntries, sum);
  //   }

  //   return ret;
  // }

  // public Report MonthReport(int year, int month)
  // {
  //   var from = new DateOnly(year, month, 1).DayBeginning();
  //   var to = new DateOnly(year, month, DateTime.DaysInMonth(year, month)).DayEnd();

  //   var projectTime = _dc.Projects.Select(project => new Tuple<Project, TimeSpan>(project, _p.ProjectTime(project, from, to)))
  //                                        .ToList();
  //   var totalSum = projectTime.Sum(x => x.Item2);

  //   var report = new Report()
  //   {
  //     ProjectTime = projectTime,
  //     TotalSum = totalSum
  //   };
  //   return report;
  // }

  // public TimeSpan TotalOvertime(DateOnly to)
  // {
  //   var uncorrectedOvertime = _dc.Days.Where(day => day.Date <= to)
  //                                     .Select(day => day.RequiredTime - _p.SumWithBreak(day))
  //                                     .Sum();
  //   var correction = _dc.WorkTimeCorrectionEntries.Where(wtce => wtce.Date <= to)
  //                                                 .Sum(wtce => wtce.CorrectionTime);
  //   return uncorrectedOvertime + correction;
  // }
}
