using EmbedIO;
using EmbedIO.Routing;
using EmbedIO.WebApi;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Pt.Core.Net;

namespace Pt.Core;

public class WtOverviewController : WebApiController
{
  private const string RoutePrefix = "wt";

  private readonly WorkTimeModelIndex _index;
  private readonly DataContext _dc;
  private readonly ILogger<WtOverviewController> _log;

  public WtOverviewController(WorkTimeModelIndex index, DataContextFactory dcf, ILoggerFactory loggerFactory)
  {
    _index = index;
    _dc = dcf.Create();
    _log = loggerFactory.CreateLogger<WtOverviewController>();
  }

  [Route(HttpVerbs.Get, $"/{RoutePrefix}/{{year}}")]
  public async Task<IEnumerable<WtPayload>> Get(int year) => await Get(wt => wt.Date.Year == year);

  [Route(HttpVerbs.Get, $"/{RoutePrefix}/{{year}}/{{month}}")]
  public async Task<IEnumerable<WtPayload>> Get(int year, int month) => await Get(wt => wt.Date.Year == year && wt.Date.Month == month);

  private async Task<IEnumerable<WtPayload>> Get(Func<WorkTimeModelIndex.WorkTimeEntry, bool> selector)
  {
    var ret = new List<WtPayload>();
    foreach (var wt in _index.WorkTime.Where(wt => selector(wt)))
    {
      var markerType = (await _dc.DayMarkers.Where(dm => dm.Date == wt.Date).FirstOrDefaultAsync())?.MarkerType ?? DayMarker.Type.None;
      var payload = WtPayload.From(wt, markerType);
      payload.trackingEntries = await _dc.TrackingEntries.Where(te => te.Date == wt.Date)
                                                         .Include(te => te.Project)
                                                         .Select(te => TrackingEntryPayload.From(te))
                                                         .ToListAsync();
      ret.Add(payload);
    }

    return ret;
  }
}
