using EmbedIO;
using EmbedIO.Routing;
using EmbedIO.WebApi;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Pt.Core.Net;

public class HolidayController : WebApiController
{
  private const string RoutePrefix = "holiday";

  private readonly DataContext _dc;
  private readonly ILogger<HolidayController> _log;

  public HolidayController(DataContextFactory dcf, ILoggerFactory loggerFactory)
  {
    _dc = dcf.Create();
    _log = loggerFactory.CreateLogger<HolidayController>();
  }

  [Route(HttpVerbs.Post, $"/{RoutePrefix}")]
  public async Task Add([JsonData] HolidayPayload? payload)
  {
    if (payload is null)
    {
      throw new ArgumentException("No holiday provided");
    }

    var newEntry = payload.To();

    if (await _dc.Holidays.AnyAsync(day => day.Date == newEntry.Date))
    {
      throw new ArgumentException("Holiday already exists");
    }

    _log.LogInformation($"Adding '{newEntry.Title}' on {newEntry.Date}.");
    _dc.Holidays.Add(newEntry);
    await _dc.SaveChangesAsync();
  }

  [Route(HttpVerbs.Delete, $"/{RoutePrefix}/{{dateStr}}")]
  public async Task Delete(string dateStr)
  {
    if (!DateOnly.TryParse(dateStr, out var date))
    {
      throw new ArgumentException($"Cannot parse requested date");
    }

    var existingEntry = await _dc.Holidays.Where(day => day.Date == date)
                                          .FirstOrDefaultAsync();
    if (existingEntry is null)
    {
      throw new ArgumentException($"Holiday does not exist");
    }

    _log.LogInformation($"Deleting holiday on {date}.");
    _dc.Holidays.Remove(existingEntry);
    await _dc.SaveChangesAsync();
  }

  [Route(HttpVerbs.Get, $"/{RoutePrefix}/{{year}}")]
  public async Task<IEnumerable<HolidayPayload>> Get(int year)
  {
    return await _dc.Holidays.Where(day => day.Date.Year == year)
                             .Select(day => HolidayPayload.From(day))
                             .ToListAsync();
  }

  [Route(HttpVerbs.Put, $"/{RoutePrefix}/{{dateStr}}")]
  public async Task Update(string dateStr, [JsonData] HolidayPayload? payload)
  {
    if (payload is null)
    {
      throw new ArgumentException("No payload provided");
    }

    if (!DateOnly.TryParse(dateStr, out var date))
    {
      throw new ArgumentException($"Cannot parse requested date");
    }

    var existingEntry = await _dc.Holidays.Where(day => day.Date == date)
                                          .FirstOrDefaultAsync();
    if (existingEntry is null)
    {
      throw new ArgumentException($"Holiday does not exist");
    }

    _log.LogInformation($"Updating holiday on {date}.");
    payload.Update(existingEntry);
    await _dc.SaveChangesAsync();
  }
}
