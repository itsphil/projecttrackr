using EmbedIO;
using EmbedIO.Routing;
using EmbedIO.WebApi;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Pt.Core.Net;

public partial class OverviewController : WebApiController
{
  private const string RoutePrefix = "overview";

  private readonly DataContext _dc;
  private readonly ILogger<OverviewController> _log;

  public OverviewController(DataContextFactory dcf, ILoggerFactory loggerFactory)
  {
    _dc = dcf.Create();
    _log = loggerFactory.CreateLogger<OverviewController>();
  }

  public enum DayKind
  {
    Free,
    Holiday,
    Vacation,
    Work,
  }

  public class DayResponse
  {
    public int day { get; set; }
    public int weekDay { get; set; }
    public string kind { get; set; } = DayKind.Free.ToString();
    public string title { get; set; } = "";

    public override string ToString() => $"{kind}: {title}";
  }

  public class MonthResponse
  {
    public int month { get; set; }
    public List<DayResponse> days { get; set; } = new();
  }

  public class YearResponse
  {
    public int year { get; set; }
    public List<MonthResponse> months { get; } = new(12);
  }

  [Route(HttpVerbs.Get, $"/{RoutePrefix}/{{year}}")]
  public async Task<YearResponse> Get(int year)
  {
    var ret = new YearResponse() { year = year };
    var currentDay = new DateOnly(year, 1, 1);
    var currentMonth = new MonthResponse() { month = 1 };
    while (currentDay.Year == year)
    {
      currentMonth.days.Add(await CreateDayResponseAsync(currentDay));

      var nextDay = currentDay.AddDays(1);
      if (nextDay.Month != currentDay.Month)
      {
        ret.months.Add(currentMonth);
        currentMonth = new MonthResponse() { month = nextDay.Month };
      }

      currentDay = nextDay;
    }

    return ret;
  }

  private async Task<DayResponse> CreateDayResponseAsync(DateOnly d)
  {
    var holiday = await _dc.Holidays.Where(holiday => holiday.Date == d)
                                    .FirstOrDefaultAsync();
    if (holiday is not null)
    {
      return new DayResponse() { day = d.Day, weekDay = (int)d.DayOfWeek, kind = DayKind.Holiday.ToString(), title = holiday.Title };
    }

    var vacation = await _dc.Vacations.Where(v => d >= v.From && d <= v.To)
                                      .FirstOrDefaultAsync();
    if (vacation is not null)
    {
      return new DayResponse() { day = d.Day, weekDay = (int)d.DayOfWeek, kind = DayKind.Vacation.ToString(), title = vacation.Comment ?? "" };
    }

    return new DayResponse() { day = d.Day, weekDay = (int)d.DayOfWeek };
  }
}
