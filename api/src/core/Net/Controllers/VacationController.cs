using EmbedIO;
using EmbedIO.Routing;
using EmbedIO.WebApi;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Pt.Core;

namespace Pt.Core.Net;

public partial class VacationController : WebApiController
{
  private const string RoutePrefix = "vacation";

  private readonly DataContext _dc;
  private readonly ILogger<VacationController> _log;

  public VacationController(DataContextFactory dcf, ILoggerFactory loggerFactory)
  {
    _dc = dcf.Create();
    _log = loggerFactory.CreateLogger<VacationController>();
  }

  [Route(HttpVerbs.Post, $"/{RoutePrefix}")]
  public async Task Add([JsonData] VacationPayload? payload)
  {
    if (payload is null)
    {
      throw new ArgumentException("No vacation provided");
    }

    var newEntry = payload.To();
    if (await _dc.Vacations.AnyAsync(day => day.From == newEntry.From && day.To == newEntry.To))
    {
      throw new ArgumentException($"Vacation {newEntry} already exists");
    }

    _log.LogInformation($"Adding vacation {newEntry}.");
    await _dc.Vacations.AddAsync(newEntry);
    await _dc.SaveChangesAsync();
  }

  [Route(HttpVerbs.Delete, $"/{RoutePrefix}/{{id}}")]
  public async Task Delete(int id)
  {
    var existingEntry = await _dc.Vacations.Where(day => day.VacationId == id)
                                           .FirstOrDefaultAsync();
    if (existingEntry is null)
    {
      throw new ArgumentException($"Vacation does not exist");
    }

    _log.LogInformation($"Deleting vacation {existingEntry}.");
    _dc.Vacations.Remove(existingEntry);
    await _dc.SaveChangesAsync();
  }

  [Route(HttpVerbs.Get, $"/{RoutePrefix}/{{year}}")]
  public async Task<IEnumerable<VacationPayload>> Get(int year)
  {
    return await _dc.Vacations.Where(v => v.From.Year == year || v.To.Year == year)
                              .Select(v => new Tuple<Vacation, decimal>(v, GetEffectiveVacationDays(_dc, year, v)))
                              .Select(entry => VacationPayload.From(entry.Item1, entry.Item2))
                              .ToListAsync();
  }

  [Route(HttpVerbs.Get, $"/{RoutePrefix}/{{year}}/remaining")]
  public async Task<VacationQuotaController.Payload> GetRemainingDays(int year)
  {
    var existingQuota = await _dc.VacationQuotas.Where(q => q.Year == year)
                                                .FirstOrDefaultAsync();
    if (existingQuota is null)
    {
      throw new ArgumentException("No quota exists for this year");
    }

    var vacations = await _dc.Vacations.Where(day => day.From.Year == year || day.To.Year == year)
                                       .ToListAsync();
    var numberOfVacationDays = vacations.Sum(v => GetEffectiveVacationDays(_dc, year, v));
    var diff = existingQuota.NumberOfDays - numberOfVacationDays;
    return new VacationQuotaController.Payload() { year = year, numberOfDays = diff };
  }

  [Route(HttpVerbs.Put, $"/{RoutePrefix}/{{id}}")]
  public async Task Update(int id, [JsonData] VacationPayload? payload)
  {
    if (payload is null)
    {
      throw new ArgumentException("No payload provided");
    }

    var existingEntry = await _dc.Vacations.Where(day => day.VacationId == id)
                                           .FirstOrDefaultAsync();
    if (existingEntry is null)
    {
      var ids = await _dc.Vacations.Select(x => x.VacationId.ToString()).ToListAsync();
      throw new ArgumentException($"Vacation {id} does not exist ({string.Join(',', ids)})");
    }

    _log.LogInformation($"Updating vacation {existingEntry}.");
    payload.Update(existingEntry);
    await _dc.SaveChangesAsync();
  }

  private static decimal GetEffectiveVacationDays(DataContext dc, int year, Vacation vacation)
  {
    decimal ret = 0;
    var date = vacation.From;
    while (date != vacation.To.AddDays(1))
    {
      if (IsVacationDay(dc, year, date))
      {
        ret++;
      }
      date = date.AddDays(1);
    }

    ret -= vacation.QuotaCorrection;

    return ret;
  }

  private static bool IsVacationDay(DataContext dc, int year, DateOnly date)
  {
    if (date.Year != year) return false;
    if (date.DayOfWeek == DayOfWeek.Saturday) return false;
    if (date.DayOfWeek == DayOfWeek.Sunday) return false;
    if (dc.Holidays.Any(h => h.Date == date)) return false;
    return true;
  }
}
