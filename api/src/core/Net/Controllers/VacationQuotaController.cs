using EmbedIO;
using EmbedIO.Routing;
using EmbedIO.WebApi;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Pt.Core.Net;

public class VacationQuotaController : WebApiController
{
  private const string RoutePrefix = "vacation";

  private readonly DataContext _dc;
  private readonly ILogger<VacationQuotaController> _log;

  public VacationQuotaController(DataContextFactory dcf, ILoggerFactory loggerFactory)
  {
    _dc = dcf.Create();
    _log = loggerFactory.CreateLogger<VacationQuotaController>();
  }

  public class Payload
  {
    public int? year { get; set; }
    public decimal? numberOfDays { get; set; }

    public static Payload From(VacationQuota q)
    {
      return new Payload()
      {
        year = q.Year,
        numberOfDays = q.NumberOfDays
      };
    }

    public VacationQuota To()
    {
      if (year is null) { throw new ArgumentException("Year cannot be empty"); }
      if (numberOfDays is null) { throw new ArgumentException("Day cannot be empty"); }
      return new VacationQuota()
      {
        Year = year.Value,
        NumberOfDays = numberOfDays.Value
      };
    }

    public void Update(VacationQuota q)
    {
      if (year is not null) throw new ArgumentException("The year is primary key and cannot be updated");
      if (numberOfDays is not null) q.NumberOfDays = numberOfDays.Value;
    }
  }

  [Route(HttpVerbs.Post, $"/{RoutePrefix}/quota")]
  public async Task Add([JsonData] Payload? payload)
  {
    if (payload is null)
    {
      throw new ArgumentException("No payload provided");
    }

    var newEntry = payload.To();
    var existingEntry = await _dc.VacationQuotas.Where(q => q.Year == newEntry.Year)
                                                .FirstOrDefaultAsync();
    if (existingEntry is not null)
    {
      throw new ArgumentException("Quota already exists for this year");
    }

    _log.LogInformation($"Adding quota for year {newEntry.Year}.");
    await _dc.VacationQuotas.AddAsync(newEntry);
    await _dc.SaveChangesAsync();
  }

  [Route(HttpVerbs.Delete, $"/{RoutePrefix}/{{year}}/quota")]
  public async Task Delete(int year)
  {
    var existingEntry = await _dc.VacationQuotas.Where(q => q.Year == year)
                                                .FirstOrDefaultAsync();
    if (existingEntry is null)
    {
      throw new ArgumentException("No quota exists for this year");
    }

    _log.LogInformation($"Deleting quota for year {year}.");
    _dc.VacationQuotas.Remove(existingEntry);
    await _dc.SaveChangesAsync();
  }

  [Route(HttpVerbs.Get, $"/{RoutePrefix}/{{year}}/quota")]
  public async Task<Payload> Get(int year)
  {
    var existingEntry = await _dc.VacationQuotas.Where(q => q.Year == year)
                                                .FirstOrDefaultAsync();
    if (existingEntry is null)
    {
      throw new ArgumentException("No quota exists for this year");
    }

    return Payload.From(existingEntry);
  }

  [Route(HttpVerbs.Put, $"/{RoutePrefix}/{{year}}/quota")]
  public async Task Update(int year, [JsonData] Payload? payload)
  {
    if (payload is null)
    {
      throw new ArgumentException("No payload provided");
    }

    var existingEntry = await _dc.VacationQuotas.Where(q => q.Year == year)
                                                .FirstOrDefaultAsync();
    if (existingEntry is null)
    {
      throw new ArgumentException("No quota exists for this year");
    }

    _log.LogInformation($"Updating quota for year {year}.");
    payload.Update(existingEntry);
    await _dc.SaveChangesAsync();
  }
}
