using Microsoft.Extensions.Logging;

namespace Pt.Core.Net;

public class EmbedIoLoggingBridge : Swan.Logging.ILogger
{
  public Swan.Logging.LogLevel LogLevel => _minimumLogLevel;

  private readonly ILogger _log;

  private Swan.Logging.LogLevel _minimumLogLevel = Swan.Logging.LogLevel.Info;

  public EmbedIoLoggingBridge(string namespaze, ILoggerFactory loggerFactory)
  {
    _log = loggerFactory.CreateLogger(namespaze);
  }

  public EmbedIoLoggingBridge WithMinimumLogLevel(Swan.Logging.LogLevel level)
  {
    _minimumLogLevel = level;
    return this;
  }

  public void Dispose()
  {
    // Nothing to do
  }

  public void Log(Swan.Logging.LogMessageReceivedEventArgs logEvent)
  {
    if (logEvent.MessageType < _minimumLogLevel)
    {
      return;
    }

    switch (logEvent.MessageType)
    {
      case Swan.Logging.LogLevel.Debug: _log.LogDebug(logEvent.Message); break;
      case Swan.Logging.LogLevel.Error: _log.LogError(logEvent.Exception, logEvent.Message); break;
      case Swan.Logging.LogLevel.Fatal: _log.LogCritical(logEvent.Exception, logEvent.Message); break;
      case Swan.Logging.LogLevel.Info: _log.LogInformation(logEvent.Message); break;
      case Swan.Logging.LogLevel.None: _log.LogDebug(logEvent.Message); break;
      case Swan.Logging.LogLevel.Trace: _log.LogTrace(logEvent.Message); break;
      case Swan.Logging.LogLevel.Warning: _log.LogWarning(logEvent.Message); break;
      default: _log.LogError($"Cannot bridge log message because log level {logEvent.MessageType} is unknown"); break;
    }
  }
}
