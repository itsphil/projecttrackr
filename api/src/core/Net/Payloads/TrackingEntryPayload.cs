namespace Pt.Core.Net;

public class TrackingEntryPayload
{
  public int trackingEntryId { get; set; }
  public string date { get; set; }
  public ProjectPayload project { get; set; }
  public decimal partOfWorkDayDuration { get; set; }
  public string? comment { get; set; }

  public static TrackingEntryPayload From(TrackingEntry te)
  {
    return new TrackingEntryPayload()
    {
      trackingEntryId = te.TrackingEntryId,
      date = te.Date.ToApiString(),
      project = ProjectPayload.From(te.Project),
      partOfWorkDayDuration = te.PartOfWorkDayDuration,
      comment = te.Comment,
    };
  }
}
