namespace Pt.Core.Net;

public class HolidayPayload
{
  public string? date { get; set; }
  public string? title { get; set; }
  public decimal? partOfFullDay { get; set; }
  public string? comment { get; set; }

  public static HolidayPayload From(Holiday day)
  {
    return new HolidayPayload()
    {
      date = day.Date.ToApiString(),
      title = day.Title,
      partOfFullDay = day.PartOfFullDay,
      comment = day.Comment
    };
  }

  public Holiday To()
  {
    if (string.IsNullOrEmpty(date)) { throw new ArgumentException("Date cannot be empty"); }
    if (!date.TryParseApiDate(out var newDate))
    {
      throw new ArgumentException("Cannot parse date");
    }
    if (string.IsNullOrEmpty(title)) { throw new ArgumentException("Title cannot be empty"); }

    return new Holiday()
    {
      Date = newDate,
      Title = title,
      PartOfFullDay = partOfFullDay ?? 1,
      Comment = comment
    };
  }

  public void Update(Holiday d)
  {
    if (date is not null) throw new ArgumentException("The date is primary key and cannot be updated");
    if (!string.IsNullOrEmpty(title)) d.Title = title;
    if (partOfFullDay is not null) d.PartOfFullDay = partOfFullDay.Value;
    if (comment is not null) d.Comment = comment;
  }
}
