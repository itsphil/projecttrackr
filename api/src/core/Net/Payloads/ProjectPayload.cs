namespace Pt.Core.Net;

public class ProjectPayload
{
  public int projectId { get; set; }
  public string name { get; set; } = "";
  public string number { get; set; } = "";
  public string comment { get; set; } = "";

  public static ProjectPayload From(Project project)
  {
    return new ProjectPayload()
    {
      projectId = project.ProjectId,
      name = project.Name,
      number = project.Number,
      comment = project.Comment,
    };
  }
}
