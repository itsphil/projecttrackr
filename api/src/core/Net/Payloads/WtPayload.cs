namespace Pt.Core.Net;

public class WtPayload
{
  public string date { get; set; } = "";
  public decimal worktimeHours { get; set; }
  public decimal overtimeHours { get; set; }
  public decimal totalOvertimeHours { get; set; }
  public List<TrackingEntryPayload> trackingEntries { get; set; } = new();
  public string markerType { get; set; } = "";

  public static WtPayload From(WorkTimeModelIndex.WorkTimeEntry entry, DayMarker.Type markerType)
  {
    return new WtPayload()
    {
      date = entry.Date.ToApiString(),
      worktimeHours = entry.WorktimeHours,
      overtimeHours = entry.OvertimeHours,
      totalOvertimeHours = entry.TotalOvertimeHours,
      markerType = markerType.ToString(),
    };
  }
}
