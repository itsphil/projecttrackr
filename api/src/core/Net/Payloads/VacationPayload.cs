namespace Pt.Core.Net;

public class VacationPayload
{
  public int? id { get; set; }
  public string? from { get; set; }
  public string? to { get; set; }
  public bool? isRequested { get; set; }
  public bool? isApproved { get; set; }
  public decimal? quotaCorrection { get; set; }
  public string? comment { get; set; }

  // Properties that are only read (not relevant for add or update)
  public decimal effectiveVacationDays { get; set; }

  public static VacationPayload From(Vacation v, decimal effectiveVacationDays)
  {
    return new VacationPayload()
    {
      id = v.VacationId,
      from = v.From.ToApiString(),
      to = v.To.ToApiString(),
      isRequested = v.IsRequested,
      isApproved = v.IsApproved,
      quotaCorrection = v.QuotaCorrection,
      comment = v.Comment,
      effectiveVacationDays = effectiveVacationDays
    };
  }

  public Vacation To()
  {
    if (string.IsNullOrEmpty(from)) { throw new ArgumentException("From date cannot be empty"); }
    if (!from.TryParseApiDate(out var fromDate)) { throw new ArgumentException("Cannot parse from date"); }
    if (string.IsNullOrEmpty(to)) { throw new ArgumentException("To date cannot be empty"); }
    if (!to.TryParseApiDate(out var toDate)) { throw new ArgumentException("Cannot parse to date"); }
    if (fromDate > toDate) { throw new ArgumentException("To date cannot be before from date"); }
    if (quotaCorrection < 0) { throw new ArgumentException("Quota correction must be positive"); }
    if (quotaCorrection > fromDate.TotalDaysTo(toDate)) throw new ArgumentException("Quota correction is larger than number of days");

    return new Vacation()
    {
      VacationId = id ?? default,
      From = fromDate,
      To = toDate,
      IsRequested = isRequested ?? false,
      IsApproved = isApproved ?? false,
      QuotaCorrection = quotaCorrection ?? 0,
      Comment = comment
    };
  }

  public void Update(Vacation d)
  {
    var fromDateParsed = DateOnly.TryParse(from, out var fromDate);
    var toDateParsed = DateOnly.TryParse(to, out var toDate);
    var bothDatesParsed = fromDateParsed && toDateParsed;

    if (bothDatesParsed)
    {
      if (fromDate > toDate) throw new ArgumentException("To date cannot be before from date");
      d.From = fromDate;
      d.To = toDate;
    }
    else if (fromDateParsed)
    {
      if (fromDate > d.To) throw new ArgumentException("To date cannot be before from date");
      d.From = fromDate;
    }
    else if (toDateParsed)
    {
      if (toDate < d.From) throw new ArgumentException("To date cannot be before from date");
      d.To = toDate;
    }

    if (quotaCorrection is not null)
    {
      if (quotaCorrection < 0)
        throw new ArgumentException("Quota correction must be positive");
      if (quotaCorrection > d.From.TotalDaysTo(d.To))
        throw new ArgumentException("Quota correction is larger than number of days");
      d.QuotaCorrection = quotaCorrection.Value;
    }

    if (isRequested is not null) d.IsRequested = isRequested.Value;
    if (isApproved is not null) d.IsApproved = isApproved.Value;
    if (comment is not null) d.Comment = comment;
  }
}
