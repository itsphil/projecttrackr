using EmbedIO;
using EmbedIO.Actions;
using EmbedIO.WebApi;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Pt.Core.Net;

public class ApiServer : IDisposable
{
  private readonly WebServer _server;

  private ApiServer(WebServer server)
  {
    _server = server;
  }

  public static async Task<ApiServer> CreateAsync(ConfigService configService, ILoggerFactory loggerFactory)
  {
    var log = loggerFactory.CreateLogger<WebServer>();

    log.LogInformation($"Starting {nameof(ApiServer)}.");

    log.LogInformation($"Configuring {nameof(ApiServer)} logging.");
    try
    {
      Swan.Logging.Logger.UnregisterLogger<Swan.Logging.ConsoleLogger>();
    }
    catch
    {
      log.LogWarning("Failed to unregister embedIO logger. Who cares...");
    }
    Swan.Logging.Logger.RegisterLogger(new EmbedIoLoggingBridge(nameof(ApiServer), loggerFactory)
                                          .WithMinimumLogLevel(Swan.Logging.LogLevel.Warning));

    var dbFileFullPath = Path.GetFullPath(configService.Get(c => c.Database.FilePath));
    log.LogInformation($"Expecting database at '{dbFileFullPath}'.");
    var dcFactory = new DataContextFactory(new DbContextOptionsBuilder<DataContext>()
                                              .UseSqlite($"Data Source={dbFileFullPath}")
                                              .Options);

    var index = new WorkTimeModelIndex(dcFactory, configService, loggerFactory);
    await index.CreateAsync();

    var port = configService.Get(c => c.Server.Port);

    var server = new WebServer(o => o.WithUrlPrefix($"http://*:{port}/api")
                                  .WithMode(HttpListenerMode.EmbedIO))
           .WithCors()
           .WithWebApi("/api",
                       m => m.WithController(() => new HolidayController(dcFactory, loggerFactory))
                             .WithController(() => new OverviewController(dcFactory, loggerFactory))
                             .WithController(() => new VacationController(dcFactory, loggerFactory))
                             .WithController(() => new VacationQuotaController(dcFactory, loggerFactory))
                             .WithController(() => new WtOverviewController(index, dcFactory, loggerFactory)))
           .WithModule(new ActionModule("/", HttpVerbs.Any, ctx => ctx.SendDataAsync(new { Message = "Ok" })));

    server.HandleUnhandledException(async (context, ex) =>
    {
      await context.SendDataAsync(new { message = ex.Message });
    });

    server.StateChanged += (s, e) => log.LogInformation($"Now in state '{e.NewState}'.");
    log.LogInformation($"API server configured on port {port}.");

    return new ApiServer(server);
  }

  public void Dispose()
  {
    _server.Dispose();
  }

  public async Task RunAsync() => await _server.RunAsync();
}
