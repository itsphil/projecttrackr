using System.Diagnostics.CodeAnalysis;

namespace Pt.Core;

public static class DateTimeExtensions
{
  public static DateTime DayBeginning(this DateOnly d)
    => d.ToDateTime(new TimeOnly(0, 0, 0));

  public static DateTime DayEnd(this DateOnly d)
    => d.ToDateTime(new TimeOnly(23, 59, 59));

  public static DateTime ToDateTime(this DateOnly d, TimeOnly t)
    => new DateTime(d.Year, d.Month, d.Day, t.Hour, t.Minute, t.Second);

  /// <summary>
  /// Return days between (from, to), i.e. not including from and to
  /// </summary>
  /// <param name="from"></param>
  /// <param name="to"></param>
  /// <returns></returns>
  public static IEnumerable<DateOnly> DaysBetween(this DateOnly from, DateOnly to)
  {
    if (from == to)
    {
      yield break;
    }

    if (from > to)
    {
      var too = to;
      to = from;
      from = too;
    }

    var d = from;
    while (d != to)
    {
      d = d.AddDays(1);
      if (d == to)
      {
        yield break;
      }
      yield return d;
    }
  }

  /// <summary>
  /// Return total number of days in range [from, to], i.e. including from and to
  /// </summary>
  /// <param name="from"></param>
  /// <param name="to"></param>
  /// <returns></returns>
  public static int TotalDaysTo(this DateOnly from, DateOnly to)
  {
    if (from > to)
    {
      throw new ArgumentException("from cannot be behind to");
    }

    var ret = 1;
    var d = from;
    while (d != to)
    {
      d = d.AddDays(1);
      ret++;
    }
    return ret;
  }

  public static TimeSpan Sum(this IEnumerable<TimeSpan> e)
  {
    var sum = new TimeSpan();
    foreach (var t in e)
    {
      sum += t;
    }
    return sum;
  }

  public static TimeSpan Sum<T>(this IEnumerable<T> e, Func<T, TimeSpan> f)
  {
    var sum = new TimeSpan();
    foreach (var ee in e)
    {
      sum += f(ee);
    }
    return sum;
  }

  public static string ToApiString(this DateOnly d) => d.ToString("yyyy-MM-dd");

  public static bool TryParseApiDate(this string d, out DateOnly date)
  {
    if (DateOnly.TryParse(d, out date))
    {
      return true;
    }

    if (DateTime.TryParse(d, out var dateTime))
    {
      date = DateOnly.FromDateTime(dateTime);
      return true;
    }

    return false;
  }
}

