using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace Pt.Core;

public class DayMarker
{
  public enum Type
  {
    None = 0,
    Office = 1,
    MobileWork = 2,
    Sick = 3,
    Travel = 4,
  }

  [Key]
  public DateOnly Date { get; set; }
  public Type MarkerType { get; set; }
  public string? Comment { get; set; }
}

public class Holiday
{
  [Key]
  public DateOnly Date { get; set; }
  public string Title { get; set; }
  public decimal PartOfFullDay { get; set; } = 1; // Rare case: a holiday may be worth only 1/2 day
  public string? Comment { get; set; }
}

public class Project
{
  [Key]
  public int ProjectId { get; set; }
  public string Name { get; set; } = "";
  public string Number { get; set; } = "";
  public string Comment { get; set; } = "";
}

public class TrackingEntry
{
  [Key]
  public int TrackingEntryId { get; set; }
  public DateOnly Date { get; set; }
  public Project Project { get; set; }
  public decimal PartOfWorkDayDuration { get; set; } = 1; // Ratio 0..1, no percentage!
  public string? Comment { get; set; }
}

public class Vacation
{
  [Key]
  public int VacationId { get; set; }
  public DateOnly From { get; set; }
  public DateOnly To { get; set; }
  public bool IsRequested { get; set; } = false;
  public bool IsApproved { get; set; } = false;
  public decimal QuotaCorrection { get; set; } = 0;
  public string? Comment { get; set; }

  public override string ToString() => $"from {From.ToApiString()} to {To.ToApiString()}";
}

public class VacationQuota
{
  [Key]
  public int Year { get; set; }
  public decimal NumberOfDays { get; set; }
}

public class WorkDay
{
  [Key]
  public DateOnly Date { get; set; }
  public string? Comment { get; set; }
  public TimeOnly From { get; set; }
  public TimeOnly To { get; set; }
}

public class WorkTimeCorrectionEntry
{
  [Key]
  public DateOnly Date { get; set; }
  public TimeSpan CorrectionTime { get; set; }
}
