using System.Reflection.Emit;
using Microsoft.EntityFrameworkCore;

namespace Pt.Core.Scaffolding;

public class EfBuildContext : DataContext
{
  public EfBuildContext()
    : base(new DbContextOptionsBuilder<DataContext>()
              .UseSqlite("Data Source=../../pt.db")
              .Options)
  { }

  protected override void OnModelCreating(ModelBuilder modelBuilder)
  {
    base.OnModelCreating(modelBuilder);

    var project1 = new Project() { ProjectId = 1, Name = "Project 1", Number = "123-456" };
    var project2 = new Project() { ProjectId = 2, Name = "Project 2", Number = "xxx" };
    var project3 = new Project() { ProjectId = 3, Name = "Home Working", Number = "56dd55" };

    modelBuilder.Entity<DayMarker>().HasData(new List<DayMarker>()
    {
      new() { Date = new DateOnly(2023, 10, 16), MarkerType = DayMarker.Type.MobileWork },
      new() { Date = new DateOnly(2023, 10, 17), MarkerType = DayMarker.Type.Office },
      new() { Date = new DateOnly(2023, 10, 18), MarkerType = DayMarker.Type.MobileWork },
      new() { Date = new DateOnly(2023, 10, 19), MarkerType = DayMarker.Type.Office },
      new() { Date = new DateOnly(2023, 10, 20), MarkerType = DayMarker.Type.Sick },
    });

    modelBuilder.Entity<Holiday>().HasData(new List<Holiday>()
    {
      new() { Date = new DateOnly(2023, 5, 1), Title = "Holiday 1" },
      new() { Date = new DateOnly(2023, 5, 2), Title = "Holiday 2" },
      new() { Date = new DateOnly(2023, 6, 30), Title = "June holiday", Comment = "well deserved" },
    });

    modelBuilder.Entity<Vacation>().HasData(new List<Vacation>()
    {
      new (){ VacationId = 1, From = new DateOnly(2023, 7, 5),  To = new DateOnly(2023, 7, 7),  Comment = "Far away" },
      new (){ VacationId = 2, From = new DateOnly(2023, 8, 16), To = new DateOnly(2023, 8, 17), Comment = "asdf" },
      new (){ VacationId = 3, From = new DateOnly(2023, 11, 6), To = new DateOnly(2023, 11, 6) },
    });

    modelBuilder.Entity<VacationQuota>().HasData(new List<VacationQuota>()
    {
      new (){ Year = 2023, NumberOfDays = 30 }
    });

    modelBuilder.Entity<Project>().HasData(new List<Project>()
    {
      project1,
      project2,
      project3
    });

    modelBuilder.Entity<TrackingEntry>().HasData(new List<object>()
    {
      new { TrackingEntryId = 1, Date = new DateOnly(2023, 10, 16), ProjectId = project1.ProjectId, PartOfWorkDayDuration = 1.0m },
      new { TrackingEntryId = 2, Date = new DateOnly(2023, 10, 17), ProjectId = project1.ProjectId, PartOfWorkDayDuration = 0.6m },
      new { TrackingEntryId = 3, Date = new DateOnly(2023, 10, 17), ProjectId = project2.ProjectId, PartOfWorkDayDuration = 0.4m },
      new { TrackingEntryId = 4, Date = new DateOnly(2023, 10, 18), ProjectId = project1.ProjectId, PartOfWorkDayDuration = 0.4m },
      new { TrackingEntryId = 5, Date = new DateOnly(2023, 10, 18), ProjectId = project3.ProjectId, PartOfWorkDayDuration = 0.4m },
    });

    modelBuilder.Entity<WorkDay>().HasData(new List<WorkDay>()
    {
      new () { Date = new DateOnly(2023, 10, 16), From = new TimeOnly(7, 45), To = new TimeOnly(16, 0) },
      new () { Date = new DateOnly(2023, 10, 17), From = new TimeOnly(6, 30), To = new TimeOnly(16, 0) },
      new () { Date = new DateOnly(2023, 10, 18), From = new TimeOnly(6, 22), To = new TimeOnly(15, 30) },
      new () { Date = new DateOnly(2023, 10, 19), From = new TimeOnly(7, 45), To = new TimeOnly(12, 0) },
    });
  }
}
