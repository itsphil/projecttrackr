using Microsoft.EntityFrameworkCore;

namespace Pt.Core;

public class DataContextFactory
{
  private readonly DbContextOptions<DataContext> _options;

  public DataContextFactory(DbContextOptions<DataContext> options)
  {
    _options = options;
  }

  public DataContext Create() => new DataContext(_options);
}
