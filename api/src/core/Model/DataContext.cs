using Microsoft.EntityFrameworkCore;

namespace Pt.Core;

public class DataContext : DbContext
{
  public DbSet<DayMarker> DayMarkers { get; set; }
  public DbSet<Holiday> Holidays { get; set; }
  public DbSet<Project> Projects { get; set; }
  public DbSet<TrackingEntry> TrackingEntries { get; set; }
  public DbSet<WorkDay> WorkDays { get; set; }
  public DbSet<WorkTimeCorrectionEntry> WorkTimeCorrectionEntries { get; set; }
  public DbSet<Vacation> Vacations { get; set; }
  public DbSet<VacationQuota> VacationQuotas { get; set; }

  public DataContext(DbContextOptions<DataContext> options)
    : base(options)
  { }

}
