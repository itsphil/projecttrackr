using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Pt.Core;

public class WorkTimeModelIndex
{

  public struct WorkTimeEntry
  {
    public DateOnly Date { get; set; }

    /// <summary>
    /// Work time of the current day reduced by the pause duration for that day.
    /// </summary>
    public decimal WorktimeHours { get; set; }

    /// <summary>
    /// Overtime of the current day.
    /// </summary>
    public decimal OvertimeHours { get; set; }

    /// <summary>
    /// Sum of the overtime since the beginning. This contains the overtime from each day until
    /// the current day and all the work time correction entries until the current day.
    /// </summary>
    public decimal TotalOvertimeHours { get; set; }
  }

  public List<WorkTimeEntry> WorkTime { get; } = new();

  private readonly DataContext _dc;
  private readonly ConfigService _configService;
  private readonly ILogger<WorkTimeModelIndex> _log;

  public WorkTimeModelIndex(DataContextFactory dcf, ConfigService configService, ILoggerFactory loggerFactory)
  {
    _dc = dcf.Create();
    _configService = configService;
    _log = loggerFactory.CreateLogger<WorkTimeModelIndex>();
  }

  public async Task CreateAsync() => await RecreateAsync();

  public async Task RecreateAsync()
  {
    WorkTime.Clear();

    decimal totalOvertimeHours = 0;
    var dcDays = await _dc.WorkDays.OrderBy(day => day.Date)
                                   .ToListAsync();
    var dcCorrections = await _dc.WorkTimeCorrectionEntries.OrderBy(entry => entry.Date)
                                                           .ToListAsync();
    var requiredHoursPerDay = _configService.Get(c => c.WorkTime.HoursPerDay);

    var dcDayIndex = 0;
    var dcCorrectionsIndex = 0;
    var date = dcDays.First().Date;
    while (date != dcDays.Last().Date)
    {
      decimal workTimeHours = 0;
      decimal overtimeHours = 0;
      if (dcDayIndex < dcDays.Count && dcDays[dcDayIndex].Date == date)
      {
        var dcDay = dcDays[dcDayIndex];
        workTimeHours = SumWithBreak(dcDay);
        overtimeHours = workTimeHours - requiredHoursPerDay;
        totalOvertimeHours += overtimeHours;

        dcDayIndex++;
      }

      if (dcCorrectionsIndex < dcCorrections.Count)
      {
        totalOvertimeHours += (decimal)dcCorrections[dcCorrectionsIndex].CorrectionTime.TotalHours;
        dcCorrectionsIndex++;
      }

      WorkTime.Add(new()
      {
        Date = date,
        WorktimeHours = workTimeHours,
        OvertimeHours = overtimeHours,
        TotalOvertimeHours = totalOvertimeHours
      });

      date = date.AddDays(1);
    }

  }

  internal decimal SumWithBreak(WorkDay day)
  {
    var hoursWithoutBreak = (decimal)(day.Date.ToDateTime(day.To) - day.Date.ToDateTime(day.From)).TotalHours;
    if (hoursWithoutBreak > _configService.Get(c => c.WorkTime.MinimumHoursForAutomaticBreak))
    {
      return hoursWithoutBreak - _configService.Get(c => c.WorkTime.BreakDuration);
    }

    return (decimal)hoursWithoutBreak;
  }
}
