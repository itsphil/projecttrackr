using Microsoft.Extensions.Logging;
using Pt.Core;
using Pt.Core.Net;

namespace Pt.Api;

internal class Program
{
  private static async Task Main(string[] args)
  {
    using var loggerFactory = LoggerFactory.Create(builder =>
    {
      builder.AddFilter("Microsoft", LogLevel.Warning)
             .AddFilter("System", LogLevel.Warning)
             .SetMinimumLevel(LogLevel.Debug)
             .AddSimpleConsole(options =>
             {
               options.IncludeScopes = true;
               options.SingleLine = true;
               options.TimestampFormat = "HH:mm:ss ";
             });
    });

    var configService = CreateConfigService();

    var mainLogger = loggerFactory.CreateLogger("Main");
    mainLogger.LogInformation("Application starting up");

    using var apiServer = await ApiServer.CreateAsync(configService, loggerFactory);
    await apiServer.RunAsync();
  }

  private static ConfigService CreateConfigService()
  {
    var config = new Config();
    config.Database.FilePath = "../pt.db";
    config.Server.Port = 4000;
    return ConfigService.CreateWithConfig(config);
  }
}
